package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnNewGame;
    Button btnSquare[][] = new Button[3][3];
    int squareValue[][] = new int[3][3];
    TextView hintText;
    boolean firstPlayerTurn = true;
    int count;
    int gameWin = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponent();
        setHint();
        for(int j = 0; j < 3; j++){
            for(int i =  0; i < 3; i++){ btnSquare[i][j].setOnClickListener(this); }
        }
        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
        if(savedInstanceState != null){ loadState(savedInstanceState); }
    }

    public void initComponent(){
        btnNewGame = findViewById(R.id.btnNewGame);
        btnSquare[0][0] = findViewById(R.id.btn1);
        btnSquare[0][1] = findViewById(R.id.btn2);
        btnSquare[0][2] = findViewById(R.id.btn3);
        btnSquare[1][0] = findViewById(R.id.btn4);
        btnSquare[1][1] = findViewById(R.id.btn5);
        btnSquare[1][2] = findViewById(R.id.btn6);
        btnSquare[2][0] = findViewById(R.id.btn7);
        btnSquare[2][1] = findViewById(R.id.btn8);
        btnSquare[2][2] = findViewById(R.id.btn9);
        hintText = findViewById(R.id.txtHint);
    }

    public void resetGame(){
        firstPlayerTurn = true;
        count = 0;
        gameWin = 0;
        for(int j = 0; j < 3; j++){
            for(int i =  0; i < 3; i++){
                squareValue[i][j] = 0;
                btnSquare[i][j].setEnabled(true);
                btnSquare[i][j].setBackgroundColor(getResources().getColor(R.color.colorLightGrey));
            }
        }
        loadUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("row0",squareValue[0]);
        outState.putIntArray("row1",squareValue[1]);
        outState.putIntArray("row2",squareValue[2]);
        outState.putBoolean("turn",firstPlayerTurn);
        outState.putInt("count",count);
        outState.putInt("gameWin",gameWin);
    }

    public void loadState(Bundle savedData){
        squareValue[0] = savedData.getIntArray("row0");
        squareValue[1] = savedData.getIntArray("row1");
        squareValue[2] = savedData.getIntArray("row2");
        firstPlayerTurn = savedData.getBoolean("turn");
        count = savedData.getInt("count");
        gameWin = savedData.getInt("gameWin");
        loadUI();
    }

    public void loadUI(){
        for(int j = 0; j < 3; j++){
            for(int i =  0; i < 3; i++){
                if(squareValue[i][j] == 1){
                    btnSquare[i][j].setText("O");
                    btnSquare[i][j].setEnabled(false);
                }
                else if(squareValue[i][j] == 2){
                    btnSquare[i][j].setText("X");
                    btnSquare[i][j].setEnabled(false);
                }
                else{
                    btnSquare[i][j].setText("");
                }
            }
        }
        if(count == 9)
        {
            btnNewGame.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }else{
            btnNewGame.setBackgroundColor(getResources().getColor(R.color.colorLightGrey));
        }
        checkWinner();

    }

    @Override
    public void onClick(View v){
        int playerValue = 0;
        boolean success = false;
        if(firstPlayerTurn){
            playerValue = 1;
        }
        else{
            playerValue = 2;
        }

        switch(v.getId()){
            case R.id.btn1:
                if(squareValue[0][0] == 0){ squareValue[0][0] = playerValue; success = true;} break;
            case R.id.btn2:
                if(squareValue[0][1] == 0){ squareValue[0][1] = playerValue; success = true;} break;
            case R.id.btn3:
                if(squareValue[0][2] == 0){ squareValue[0][2] = playerValue; success = true;} break;
            case R.id.btn4:
                if(squareValue[1][0] == 0){ squareValue[1][0] = playerValue; success = true;} break;
            case R.id.btn5:
                if(squareValue[1][1] == 0){ squareValue[1][1] = playerValue; success = true;} break;
            case R.id.btn6:
                if(squareValue[1][2] == 0){ squareValue[1][2] = playerValue; success = true;}break;
            case R.id.btn7:
                if(squareValue[2][0] == 0){ squareValue[2][0] = playerValue; success = true;}break;
            case R.id.btn8:
                if(squareValue[2][1] == 0){ squareValue[2][1] = playerValue; success = true;}break;
            case R.id.btn9:
                if(squareValue[2][2] == 0){ squareValue[2][2] = playerValue; success = true;}break;
        }

        if(success){
            if(firstPlayerTurn){
                ((Button)v).setText("O");
            }
            else{
                ((Button)v).setText("X");
            }
            ((Button)v).setEnabled(false);
            firstPlayerTurn = !firstPlayerTurn;
            count++;
            if(count == 9){
                btnNewGame.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }
        }
        checkWinner();
    }

    public void checkWinner(){
        if(squareValue[0][0] == squareValue[0][1] && squareValue[0][0] == squareValue[0][2] && squareValue[0][0]!=0){
            btnSquare[0][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[0][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[0][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[0][0];
        }
        else if (squareValue[1][0] == squareValue[1][1] && squareValue[1][0] == squareValue[1][2] && squareValue[1][0]!=0){
            btnSquare[1][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[1][0];
        }
        else if (squareValue[2][0] == squareValue[2][1] && squareValue[2][0] == squareValue[2][2] && squareValue[2][0]!=0){
            btnSquare[2][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[2][0];
        }
        else if (squareValue[0][0] == squareValue[1][0] && squareValue[0][0] == squareValue[2][0] && squareValue[0][0]!=0){
            btnSquare[0][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[0][0];
        }
        else if (squareValue[0][1] == squareValue[1][1] && squareValue[0][1] == squareValue[2][1] && squareValue[0][1]!=0){
            btnSquare[0][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[0][1];
        }
        else if (squareValue[0][2] == squareValue[1][2] && squareValue[0][2] == squareValue[2][2] && squareValue[0][2]!=0){
            btnSquare[0][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[0][2];
        }
        else if (squareValue[0][0] == squareValue[1][1] && squareValue[0][0] == squareValue[2][2] && squareValue[0][0]!=0){
            btnSquare[0][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[0][0];
        }
        else if (squareValue[0][2] == squareValue[1][1] && squareValue[0][2] == squareValue[2][0] && squareValue[0][2]!=0){
            btnSquare[0][2].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[1][1].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btnSquare[2][0].setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            gameWin = squareValue[0][2];
        }
        setHint();
    }

    public void setHint(){
        if(gameWin == 1){
            hintText.setText("Player O Win");
            disableGame();
        }
        else if(gameWin == 2){
            hintText.setText("Player X Win");
            disableGame();
        }
        else if(firstPlayerTurn){
            hintText.setText("Player O's Turn");
        }
        else if(count == 9){
            hintText.setText("Match Tie");
        }
        else{
            hintText.setText("Player X's Turn");
        }
    }

    public void disableGame(){
        for(int j = 0; j < 3; j++){
            for(int i =  0; i < 3; i++){
                btnSquare[i][j].setEnabled(false);
                btnNewGame.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }
        }
    }


}
